<?php
include 'head.inc.html';
$comissao = true;
include 'navbar.inc.php';
?>
            <div class="row">
              <div class="col-sm-6 col-md-4">
                <div class="thumbnail">
                  <img src="img/alessandro.jpg" class="img-responsive img-rounded">
                  <div class="caption">
                    <h4>Alessandro de Fraga Collioni</h4>
                    <p>Ciência da Computação</p>
                  </div>
                </div>
              </div>
              <div class="col-sm-6 col-md-4">
                <div class="thumbnail">
                  <img src="img/caio.jpg" class="img-responsive img-rounded">
                  <div class="caption">
                    <h4>Caio Steglich Borges</h4>
                    <p>Sistemas de Informação</p>
                  </div>
                </div>
              </div>
              <div class="col-sm-6 col-md-4">
                <div class="thumbnail">
                  <img src="img/francis.jpg" class="img-responsive img-rounded">
                  <div class="caption">
                    <h4>Francis Vagner dos Anjos Fontoura</h4>
                    <p>Sistemas de Informação</p>
                  </div>
                </div>
              </div>
              <div class="col-sm-6 col-md-4">
                <div class="thumbnail">
                  <img src="img/ildevana.jpg" class="img-responsive img-rounded">
                  <div class="caption">
                    <h4>Ildevana Poltronieri Rodrigues</h4>
                    <p>Sistemas de Informação</p>
                  </div>
                </div>
              </div>
              <div class="col-sm-6 col-md-4">
                <div class="thumbnail">
                  <img src="img/jocines.jpg" class="img-responsive img-rounded">
                  <div class="caption">
                    <h4>Jocines Dela-Flora da Silveira</h4>
                    <p>Sistemas de Informação</p>
                  </div>
                </div>
              </div>
              <div class="col-sm-6 col-md-4">
                <div class="thumbnail">
                  <img src="img/samuel.jpg" class="img-responsive img-rounded">
                  <div class="caption">
                    <h4>Samuel Franca Domingues</h4>
                    <p>Sistemas de Informação</p>
                  </div>
                </div>
              </div>
              <div class="col-sm-6 col-sm-offset-3 col-md-4 col-md-offset-4">
                <div class="thumbnail">
                  <img src="img/jb.jpg" class="img-responsive img-rounded">
                  <div class="caption">
                    <h4>João Batista Souza de Oliveira</h4>
                    <p>Professor</p>
                  </div>
                </div>
              </div>
            </div>
<?php
include 'foot.inc.html';
?>