        <div class="navbar navbar-default navbar-fixed-top">
            <div class="container">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="index.php"><b><span style="color: rgb(80,80,80)">JA</span><span style="color: rgb(20,90,150)">FAC</span><span style="color: rgb(100,190,240)">IN</span><span style="color: rgb(150,30,30)">2014</span></b></a>
                </div>
                <div class="collapse navbar-collapse navbar-ex1-collapse">
                    <ul class="nav navbar-left navbar-nav">
                        <li<?=($index)?' class="active"':''?>>
                            <a href="<?=($index)?'#':'index.php'?>">Página inicial</a>
                        </li>
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown">Edição 2014 <span class="caret"></span></a>
                            <ul class="dropdown-menu" role="menu">
                                <li<?=($participar)?' class="active"':''?>>
                                    <a href="<?=($participar)?'#':'como-participar.php'?>">Como participar?</a>
                                </li>
                                <li<?=($comissao)?' class="active"':''?>>
                                    <a href="<?=($comissao)?'#':'comissao-organizadora.php'?>">Comissão organizadora</a>
                                </li>
                            </ul>
                        </li>
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown">Atividades <span class="badge">1</span><span class="caret"></span></a>
                            <ul class="dropdown-menu" role="menu">
                                <li<?=($palestras)?' class="active"':''?>>
                                    <a href="<?=($palestras)?'#':'palestras-e-paineis.php'?>">Palestras e Painéis <span class="badge">1</span></a>
                                </li>
                                <li<?=($minicursos)?' class="active"':''?>>
                                    <a href="<?=($minicursos)?'#':'minicursos.php'?>">Minicursos</a>
                                </li>
                            </ul>
                        </li>
                    </ul>
                    <ul class="nav navbar-nav navbar-right">
                        <li class="disabled">
                            <a href="http://webappcl.pucrs.br/inscricoes/inscricao?projeto=c648082ae6aaea45" target="_blank">
                                Inscrições <i>Online</i> <span class="glyphicon glyphicon-new-window"></span>
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="container">
