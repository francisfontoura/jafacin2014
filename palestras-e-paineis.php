<?php
include 'head.inc.html';
$palestras = true;
include 'navbar.inc.php';
?>
            <style>.btn{white-space: normal}</style>
            <div class="row">
                <div class="col-sm-4">
                    <div class="well">Terça, 23/9</div>
                    <div class="panel panel-default">
                        <div class="panel-heading">JK (17:35-19:00)</div>
                        <div class="panel-body">
                            <button class="btn btn-default btn-block" data-toggle="collapse" data-target=".realidade-aumentada">
                                <div class="collapse in realidade-aumentada">Realidade Aumentada<br><small>Auditório Térreo (260 vagas)</small></div>
                                <div class="collapse realidade-aumentada">Palestrante<br><small>Prof. Dr. Márcio Sarroglia Pinho</small></div>
                                <span class="caret"></span>
                            </button>
                        </div>
                    </div>
                    <div class="panel panel-default">
                        <div class="panel-heading">LM (19:30-21:00)</div>
                        <div class="panel-body">
                            <button class="btn btn-default btn-block" data-toggle="collapse" data-target=".big-data">
                                <div class="collapse in big-data">Big Data e a Internet das Coisas<br><small>Auditório Térreo (260 vagas)</small></div>
                                <div class="collapse big-data">Palestrante<br><small>Matias Schertel</small></div>
                                <span class="caret"></span>
                            </button>
                            <button class="btn btn-default btn-block" data-toggle="collapse" data-target=".mobilidade">
                                <div class="collapse in mobilidade">Painel: Mobilidade Acadêmica<br><small>Sala 517 (87 vagas)</small></div>
                                <div class="collapse mobilidade">Sem informações adicionais.</div>
                                <span class="caret"></span>
                            </button>
                            <button class="btn btn-default btn-block" data-toggle="collapse" data-target=".student-partner">
                                <div class="collapse in student-partner">Student Partner e inserção em comunidades Microsoft<br><small>Sala 516 (70 vagas)</small></div>
                                <div class="collapse student-partner">Palestrantes<br><small>Lucas Chies<br>Marcos Freccia</small></div>
                                <span class="caret"></span>
                            </button>
                        </div>
                    </div>
                    <div class="panel panel-default">
                        <div class="panel-heading">NP (21:15-22:45)</div>
                        <div class="panel-body">
                            <button class="btn btn-default btn-block" data-toggle="collapse" data-target=".empreendedorismo">
                                <div class="collapse in empreendedorismo">Empreendedorismo com Causa/Casa Liberdade<br><small>Auditório Térreo (260 vagas)</small></div>
                                <div class="collapse empreendedorismo">Palestrantes<br><small>Daniel Wildt<br>Felipe Benites Cabral</small></div>
                                <span class="caret"></span>
                            </button>
                            <button class="btn btn-default btn-block" data-toggle="collapse" data-target=".carreira-gp">
                                <div class="collapse in carreira-gp">Carreira de Gerente de Projetos<br><small>Sala 516 (70 vagas)</small></div>
                                <div class="collapse carreira-gp">Palestrante<br><small>Thiago Regal da Silva</small></div>
                                <span class="caret"></span>
                            </button>
                        </div>
                    </div>
                </div>
                <div class="col-sm-4">
                    <div class="well">Quarta, 24/9</div>
                    <div class="panel panel-default">
                        <div class="panel-heading">JK (17:35-19:00)</div>
                        <div class="panel-body">
                            <button class="btn btn-default btn-block" data-toggle="collapse" data-target=".criptografia">
                                <div class="collapse in criptografia">Criptografia e Segurança<br><small>Auditório Térreo (260 vagas)</small></div>
                                <div class="collapse criptografia">Palestrante<br><small>Prof. Dr. Avelino Francisco Zorzo</small></div>
                                <span class="caret"></span>
                            </button>
                        </div>
                    </div>
                    <div class="panel panel-default">
                        <div class="panel-heading">LM (19:30-21:00) <span class="badge">1</span></div>
                        <div class="panel-body">
                            <button class="btn btn-default btn-block" data-toggle="collapse" data-target=".interfaces-multimodais">
                                <div class="collapse in interfaces-multimodais">Interfaces multimodais para aprender e pensar<br><small>Auditório Térreo (260 vagas)</small></div>
                                <div class="collapse interfaces-multimodais">Palestrante<br><small>Prof. Dr. Jaime Sánchez</small></div>
                                <span class="caret"></span>
                            </button>
                            <button class="btn btn-default btn-block" data-toggle="collapse" data-target=".testing-cup">
                                <div class="collapse in testing-cup">Software Testing World Cup: Quem são os melhores testers do mundo?<br><small>Sala 517 (87 vagas)</small></div>
                                <div class="collapse testing-cup">Palestrantes<br><small>Eduardo Oliveira<br>Beatriz Bergamaschi<br>Rafael Mendonça<br>Danilo Guimarães</small></div>
                            </button>
                            <button class="btn btn-default btn-block" data-toggle="collapse" data-target=".palavras-magicas">
                                <span class="badge">adic. 1/9</span>
                                <div class="collapse in palavras-magicas">XP, Kanban e outras palavras mágicas<br><small>Sala 516 (70 vagas)</small></div>
                                <div class="collapse palavras-magicas">Palestrantes<br><small>Leonardo Cassuriaga<br>Felipe Foliatti</small></div>
                                <span class="caret"></span>
                            </button>
                        </div>
                    </div>
                    <div class="panel panel-default">
                        <div class="panel-heading">NP (21:15-22:45)</div>
                        <div class="panel-body">
                            <button class="btn btn-default btn-block" data-toggle="collapse" data-target=".gamification">
                                <div class="collapse in gamification">Gamification<br><small>Auditório Térreo (260 vagas)</small></div>
                                <div class="collapse gamification">Palestrante<br><small>Fernando Peña D'Andrea</small></div>
                                <span class="caret"></span>
                            </button>
                            <button class="btn btn-default btn-block" data-toggle="collapse" data-target=".planejamento">
                                <div class="collapse in planejamento">Planejamento de Carreira: Por que pensar nisso?<br><small>Sala 517 (87 vagas)</small></div>
                                <div class="collapse planejamento">Palestrantes<br><small>Denise Bonato<br>Rafaela Bello</small></div>
                                <span class="caret"></span>
                            </button>
                        </div>
                    </div>
                </div>
                <div class="col-sm-4">
                    <div class="well">Quinta, 25/9</div>
                    <div class="panel panel-default">
                        <div class="panel-heading">JK (17:35-19:00)</div>
                        <div class="panel-body">
                            <button class="btn btn-default btn-block" data-toggle="collapse" data-target=".marco-civil">
                                <div class="collapse in marco-civil">Marco Civil da Internet: Um olhar sob o ponto de vista técnico<br><small>Auditório Térreo (260 vagas)</small></div>
                                <div class="collapse marco-civil">Palestrante<br><small>Evandro Della Vecchia Pereira</small></div>
                                <span class="caret"></span>
                            </button>
                        </div>
                    </div>
                    <div class="panel panel-default">
                        <div class="panel-heading">LM (19:30-21:00)</div>
                        <div class="panel-body">
                            <button class="btn btn-default btn-block" data-toggle="collapse" data-target=".php">
                                <div class="collapse in php">PHP?! Mas você não disse que era programador?<br><small>Auditório Térreo (260 vagas)</small></div>
                                <div class="collapse php">Palestrante<br><small>Cristina Otto<br>PHP Developer Grupo RBS</small></div>
                                <span class="caret"></span>
                            </button>
                            <button class="btn btn-default btn-block" data-toggle="collapse" data-target=".artigo-cientifico">
                                <div class="collapse in artigo-cientifico">Escrita de Artigo Científico<br><small>Sala 517 (87 vagas)</small></div>
                                <div class="collapse artigo-cientifico">Palestrante<br><small>Prof. Dr. Felipe Rech Meneguzzi</small></div>
                                <span class="caret"></span>
                            </button>
                            <button class="btn btn-default btn-block" data-toggle="collapse" data-target=".carreira-academica">
                                <div class="collapse in carreira-academica">Carreira Acadêmica e Apresentação do Programa de Pós-Graduação PUCRS<br><small>Sala 516 (70 vagas)</small></div>
                                <div class="collapse carreira-academica">Palestrante<br><small>Prof. Dr. Luiz Gustavo Leão Fernandes</small></div>
                                <span class="caret"></span>
                            </button>
                        </div>
                    </div>
                    <div class="panel panel-default">
                        <div class="panel-heading">NP (21:15-22:00)</div>
                        <div class="panel-body">
                            <button class="btn btn-default btn-block" data-toggle="collapse" data-target=".aprender">
                                <div class="collapse in aprender">Aprender a aprender<br><small>Auditório Térreo (260 vagas)</small></div>
                                <div class="collapse aprender">Palestrante<br><small>Profa. Dra. Lucia Giraffa</small></div>
                                <span class="caret"></span>
                            </button>
                            <button class="btn btn-default btn-block" data-toggle="collapse" data-target=".mudanca-curricular">
                                <div class="collapse in mudanca-curricular">Painel: Mudança Curricular<br><small>Sala 516 (70 vagas)</small></div>
                                <div class="collapse mudanca-curricular">Sem informações adicionais.</div>
                                <span class="caret"></span>
                            </button>
                        </div>
                    </div>
                </div>
            </div>
<?php
include 'foot.inc.html';
?>