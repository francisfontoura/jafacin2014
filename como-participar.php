<?php
include 'head.inc.html';
$participar = true;
include 'navbar.inc.php';
?>
            <div class="row">
                <div class="col-md-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h1 class="panel-title">Como participar?</h1>
                        </div>
                        <div class="panel-body">
                            <p class="">Todas as atividades são gratuitas e voltadas para a comunidade acadêmica.</p>
                            <p class="">O interessado deverá inscrever-se através do <a href="http://webappcl.pucrs.br/inscricoes/inscricao?projeto=c648082ae6aaea45" target="_blank"><i>link</i> disponibilizado</a>, <b>imprimir o comprovante</b> de inscrição e, <b>de posse desse</b>, entregar 1 kg de alimento não perecível (<b>exceto açúcar e sal</b>) na sala 501, <b>durante o evento.</b></p>
                            <p class="">No ato da entrega, o aluno receberá outro comprovante, de confirmação da inscrição, que deverá ser utilizado na entrada de cada atividade, liberando seu acesso.</p>
                        </div>
                    </div>
                </div>
            </div>
<?php
include 'foot.inc.html';
?>