<?php
include 'head.inc.html';
$index = true;
include 'navbar.inc.php';
?>
            <div class="row">
                <div class="col-md-6 panel">
                    <img src="img/arte.jpg" class="img-responsive img-rounded">
                </div>
                <div class="col-md-6">
                    <div class="panel panel-default">
                        <div class="panel-heading"><h2 class="panel-title">Sobre a Jornada Acadêmica</h2></div>
                        <div class="panel-body" style="text-align: justify">
                            <p>A Jornada Acadêmica da FACIN é um evento conduzido por alunos, com o auxílio de um professor e da direção da faculdade, que ocorre anualmente e traz novidades na área de Tecnologia da Informação em um ciclo de palestras, painéis e minicursos.</p>
                            <p>Os alunos terão a oportunidade de debater sobre diversos assuntos e tendências na área.</p>
                            <p>Os temas são escolhidos a partir de sugestões de alunos e professores da Faculdade de Informática.</p>
                            <p>Este ano, o evento acontecerá <b>nos dias 23, 24 e 25 de setembro, das 17h35min às 22h45min</b>.</p>
                            <p>Nesses dias, não haverá aula para que todos possam participar. A presença nas atividades <b>fora do horário da aula</b> será contabilizada como <b>horas complementares</b>.</p>
                        </div>
                    </div>
                    <div class="panel panel-default">
                        <div class="panel-body">
                            <div class="fb-like" data-href="http://facebook.com/jafacin" data-layout="standard" data-action="like" data-show-faces="true" data-share="true"></div>
                        </div>
                    </div>
                </div>
            </div>
<?php
include 'foot.inc.html';
?>