<?php
include 'head.inc.html';
$minicursos = true;
include 'navbar.inc.php';
?>
                <table class="table table-bordered" style="table-layout: fixed">
                    <thead>
                        <tr class="active">
                            <th>Terça, 23/9</th>
                            <th>Quarta, 24/9</th>
                            <th>Quinta, 25/9</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <th colspan="3">JK (17:35-19:00)</th>
                        </tr>
                        <tr>
                            <td colspan="3" style="border-bottom: none" class="success">Scrum<br><small>Jorge Kotick Audy</small><br><small>Sala 508 (Turma única - 60 vagas)</small></td>
                        </tr>
                        <tr>
                            <td rowspan="3" style="border-top: none" class="success"></td>
                            <td class="info">Android<br><small>Ícaro Raupp Henrique</small><br><small>Sala 418 (Turma 1 - 30 vagas)</small></td>
                            <td class="info">Android<br><small>Ícaro Raupp Henrique</small><br><small>Sala 418 (Turma 2 - 30 vagas)</small></td>
                        </tr>
                        <tr>
                            <td rowspan="2" class="warning"><span style="font-family: serif">L<sup style="margin-left: -0.4em; font-size: 0.7em">A</sup>T<sub style="font-size: 1em">E</sub>X</span><br><small>Prof. Dr. Marcelo Cohen</small><br><small>Sala 401 (Turma única - 30 vagas)</small></td>
                            <th>JKLM (17:35-21:00)</th>
                        </tr>
                        <tr>
                            <td rowspan="3" class="danger">Robótica<br><small>Marcio G. Morais</small><br><small>Sala 418 (Turma única - 30 vagas)</small></td>
                        <tr>
                            <th colspan="2">LMNP (19:30-22:45)</th>
                        </tr>
                        <tr>
                            <td class="info" style="border-bottom: none">Python<br><small>Cícero Raupp Rolim</small><br><small>Sala 418 (Turma única - 30 vagas)</small></td>
                            <td class="success" style="border-bottom: none">Ruby on Rails<br><small>Jean Carlo Emer</small><br><small>Sala 418 (Turma 1 - 30 vagas)</small></td>
                        </tr>
                        <tr>
                            <td class="info" style="border-top: none; color: transparent">Python<br><small>Cícero Raupp Rolim</small><br><small>Sala 418 (Turma única - 30 vagas)</small></td>
                            <td class="success" style="border-top: none; color: transparent">Ruby on Rails<br><small>Jean Carlo Emer</small><br><small>Sala 418 (Turma 1 - 30 vagas)</small></td>
                        </tr>
                    </tbody>
                </table>
<?php
include 'foot.inc.html';
?>